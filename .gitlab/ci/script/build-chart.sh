#!/bin/bash

set -euo pipefail

source "$(dirname "$0")/utils.sh"

chart_url="https://dependabot-gitlab.gitlab.io/chart"
chart_dir="charts/dependabot-gitlab"

info "Package helm chart"
cp README.md LICENSE "$chart_dir/"
if [ -d "$chart_dir/charts" ]; then
  helm package "$chart_dir"
else
  helm package --dependency-update "$chart_dir"
fi

info "Fetch index.yaml from ${chart_url}"
curl -f -o index.yaml "${chart_url}/index.yaml"

info "Update index.yml"
helm repo index . --merge index.yaml --url https://storage.googleapis.com/dependabot-gitlab
mv index.yaml public/
