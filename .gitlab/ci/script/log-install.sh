#!/bin/bash

set -euo pipefail

source "$(dirname "$0")/utils.sh"

log_with_header "Events of namespace ${NAMESPACE}"
kubectl get events --output wide --namespace ${NAMESPACE}

for pod in $(kubectl get pods --no-headers --namespace ${NAMESPACE} --output jsonpath={.items[*].metadata.name}); do
  log_with_header "Description of pod ${pod}"
  kubectl describe pod ${pod} --namespace ${NAMESPACE}

  for container in $(kubectl get pods ${pod} --no-headers --namespace ${NAMESPACE} --output jsonpath={.spec.initContainers[*].name}); do
    log_with_header "Logs of container ${pod}/${container}" "-"
    kubectl logs ${pod} --namespace ${NAMESPACE} --container ${container}
  done

  for container in $(kubectl get pods ${pod} --no-headers --namespace ${NAMESPACE} --output jsonpath={.spec.containers[*].name}); do
    log_with_header "Logs of container ${pod}/${container}" "-"
    kubectl logs ${pod} --namespace ${NAMESPACE} --container ${container}
  done
done
