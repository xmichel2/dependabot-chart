apiVersion: {{ include "common.capabilities.deployment.apiVersion" . }}
kind: Deployment
metadata:
  name: {{ include "dependabot-gitlab.fullname" . }}-worker
  labels:
    {{- include "dependabot-gitlab.labels" . | nindent 4 }}
  {{- with .Values.worker.deploymentAnnotations }}
  annotations:
    {{- toYaml . | nindent 4 }}
  {{- end }}
spec:
  strategy: {{ toYaml .Values.worker.updateStrategy | nindent 4 }}
  replicas: {{ .Values.worker.replicaCount }}
  selector:
    matchLabels:
      {{- include "dependabot-gitlab.selectorLabels" . | nindent 6 }}
      app.kubernetes.io/component: worker
  template:
    metadata:
      labels:
        {{- include "dependabot-gitlab.selectorLabels" . | nindent 8 }}
        {{- with .Values.worker.podLabels }}
        {{ toYaml . | nindent 8 }}
        {{- end }}
        app.kubernetes.io/component: worker
      annotations:
        {{- include "dependabot-gitlab.podAnnotations" . | nindent 8 }}
        {{- with .Values.worker.podAnnotations }}
        {{ toYaml . | nindent 8 }}
        {{- end }}
    spec:
      {{- with .Values.worker.hostAliases }}
      hostAliases: 
        {{toYaml . | nindent 8}}
      {{- end }}
      serviceAccountName: {{ include "dependabot-gitlab.serviceAccountName" . }}
      {{- with .Values.podSecurityContext }}
      securityContext:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      initContainers:
        {{- include "dependabot-gitlab.migrationsWaitContainer" . | nindent 8 }}
        {{- include "dependabot-gitlab.redisWaitContainer" . | nindent 8 }}
      containers:
        - name: {{ .Chart.Name }}-worker
          {{- with .Values.worker.containerSecurityContext }}
          securityContext:
            {{- toYaml . | nindent 12 }}
          {{- end }}
          {{- include "dependabot-gitlab.image" . | nindent 10 }}
          args:
            - "sidekiq"
          env:
            - name: SETTINGS__UPDATER_TEMPLATE_PATH
              value: /home/dependabot/app/kube/templates/updater-pod.yaml
            - name: SETTINGS__DELETE_UPDATER_CONTAINER
              value: {{ .Values.updater.deleteContainer | quote }}
            - name: SETTINGS__UPDATER_CONTAINER_STARTUP_TIMEOUT
              value: {{ .Values.updater.startupDeadlineSeconds | quote }}
            - name: SETTINGS__METRICS
              value: {{ .Values.metrics.enabled | quote }}
          {{- if or .Values.worker.livenessProbe.enabled .Values.worker.startupProbe.enabled }}
            - name: SIDEKIQ_ALIVE_PORT
              value: {{ .Values.worker.probePort | quote }}
          {{- else }}
            - name: DISABLE_SIDEKIQ_ALIVE
              value: "true"
          {{- end }}
          {{- with (include "dependabot-gitlab.database-credentials" .) }}
            {{- . | nindent 12 }}
          {{- end }}
            - name: RAILS_MAX_THREADS
              value: {{ .Values.worker.maxConcurrency | quote }}
          {{- with .Values.worker.extraEnvVars }}
            {{- toYaml . | nindent 12 }}
          {{- end}}
          envFrom:
            - configMapRef:
                name: {{ include "dependabot-gitlab.fullname" . }}
            - secretRef:
                {{- if .Values.credentials.existingSecret }}
                name: {{ .Values.credentials.existingSecret }}
                {{- else }}
                name: {{ include "dependabot-gitlab.fullname" . }}
                {{- end }}
            {{- if (include "dependabot-gitlab.registries-credentials" .) }}
            - secretRef:
                {{- if .Values.registriesCredentials.existingSecret }}
                name: {{ .Values.registriesCredentials.existingSecret }}
                {{- else }}
                name: {{ include "dependabot-gitlab.fullname" . }}-registries
                {{- end }}
            {{- end }}
          {{- if .Values.worker.livenessProbe.enabled }}
          livenessProbe:
            httpGet:
              path: /healthcheck
              port: {{ .Values.worker.probePort }}
            failureThreshold: {{ .Values.worker.livenessProbe.failureThreshold }}
            periodSeconds: {{ .Values.worker.livenessProbe.periodSeconds }}
            timeoutSeconds: {{ .Values.worker.livenessProbe.timeoutSeconds }}
          {{- end }}
          {{- if .Values.worker.livenessProbe.enabled }}
          startupProbe:
            httpGet:
              path: /healthcheck
              port: {{ .Values.worker.probePort }}
            failureThreshold: {{ .Values.worker.startupProbe.failureThreshold }}
            periodSeconds: {{ .Values.worker.startupProbe.periodSeconds }}
            timeoutSeconds: {{ .Values.worker.startupProbe.timeoutSeconds }}
            initialDelaySeconds: {{ .Values.worker.startupProbe.initialDelaySeconds }}
          {{- end }}
          lifecycle:
            preStop:
              exec:
                # SIGTERM triggers a quick exit; gracefully terminate instead
                command: ["kube/sidekiq-quiet.sh"]
          {{- with .Values.worker.resources }}
          resources:
            {{- toYaml . | nindent 12 }}
          {{- end }}
          volumeMounts:
            - name: updater-template
              mountPath: /home/dependabot/app/kube/templates
              readOnly: true
          {{- if (include "dependabot-gitlab.base-config" .) }}
            {{- include "dependabot-gitlab.base-config-mount" . | nindent 12 }}
          {{- end }}
          {{- with .Values.worker.extraVolumeMounts }}
            {{- toYaml . | nindent 12 }}
          {{- end }}
      terminationGracePeriodSeconds: 300 # Large dependency files or docker images can take long time to process
      {{- with .Values.worker.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.worker.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.worker.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.image.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      volumes:
        - name: updater-template
          configMap:
            name: {{ include "dependabot-gitlab.fullname" . }}-updater-configmap
            items:
              - key: updater-pod.yaml
                path: updater-pod.yaml
      {{- if (include "dependabot-gitlab.base-config" .) }}
        {{- include "dependabot-gitlab.base-config-volume" . | nindent 8 }}
      {{- end }}
      {{- with .Values.worker.extraVolumes }}
        {{- toYaml . | nindent 8 }}
      {{- end }}
