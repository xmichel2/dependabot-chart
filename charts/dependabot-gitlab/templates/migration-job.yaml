apiVersion: batch/v1
kind: Job
metadata:
  {{- if or .Values.mongodb.enabled .Values.serviceAccount.create }}
  # if mongodb deployment or service account creation is enabled, set unique job name since it is not executed as helm hook
  name: {{ include "dependabot-gitlab.fullname" . }}-migration-{{ .Release.Revision }}
  {{- else }}
  name: {{ include "dependabot-gitlab.fullname" . }}-migration
  {{- end }}
  labels:
    {{- include "dependabot-gitlab.labels" . | nindent 4 }}
    app.kubernetes.io/component: migration-job
  {{- if and (not .Values.mongodb.enabled) (not .Values.serviceAccount.create ) }}
  annotations:
    helm.sh/hook: pre-install,pre-upgrade
  {{- end }}
spec:
  {{- if .Values.migrationJob.enableCleanup }}
  ttlSecondsAfterFinished: {{ .Values.migrationJob.ttlSecondsAfterFinished }}
  {{- end }}
  backoffLimit: {{ .Values.migrationJob.backoffLimit }}
  activeDeadlineSeconds: {{ .Values.migrationJob.activeDeadlineSeconds }}
  template:
    metadata:
      annotations:
        {{- include "dependabot-gitlab.podAnnotations" . | nindent 8 }}
        {{- with .Values.migrationJob.podAnnotations }}
        {{ toYaml . | nindent 8 }}
        {{- end }}
    spec:
      serviceAccountName: {{ include "dependabot-gitlab.serviceAccountName" . }}
      {{- with .Values.podSecurityContext }}
      securityContext:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      initContainers:
        - name: wait-db
          {{- include "dependabot-gitlab.image" . | nindent 10 }}
          {{- with .Values.migrationJobInitContainer.securityContext }}
          securityContext:
            {{- toYaml . | nindent 12 }}
          {{- end }}
          args:
            - "rake"
            - "background_tasks:check_db"
          {{- with (include "dependabot-gitlab.database-credentials" .) }}
          env:
            {{- . | nindent 12 }}
          {{- end }}
          envFrom:
            - configMapRef:
                name: {{ include "dependabot-gitlab.fullname" . }}
            - secretRef:
                {{- if .Values.credentials.existingSecret }}
                name: {{ .Values.credentials.existingSecret }}
                {{- else }}
                name: {{ include "dependabot-gitlab.fullname" . }}
                {{- end }}
            {{- if (include "dependabot-gitlab.registries-credentials" .) }}
            - secretRef:
                {{- if .Values.registriesCredentials.existingSecret }}
                name: {{ .Values.registriesCredentials.existingSecret }}
                {{- else }}
                name: {{ include "dependabot-gitlab.fullname" . }}-registries
                {{- end }}
            {{- end }}
          {{- with .Values.migrationJobInitContainer.resources }}
          resources:
            {{- toYaml . | nindent 12 }}
          {{- end }}
      containers:
        - name: {{ .Chart.Name }}-migration-job
          {{- include "dependabot-gitlab.image" . | nindent 10 }}
          {{- with .Values.migrationJob.containerSecurityContext }}
          securityContext:
            {{- toYaml . | nindent 12 }}
          {{- end }}
          args:
            - "rake"
            - "db:migrate"
          {{- with (include "dependabot-gitlab.database-credentials" .) }}
          env:
            {{- . | nindent 12 }}
          {{- end }}
          envFrom:
            - configMapRef:
                name: {{ include "dependabot-gitlab.fullname" . }}
            - secretRef:
                {{- if .Values.credentials.existingSecret }}
                name: {{ .Values.credentials.existingSecret }}
                {{- else }}
                name: {{ include "dependabot-gitlab.fullname" . }}
                {{- end }}
          {{- with .Values.migrationJob.resources }}
          resources:
            {{- toYaml . | nindent 12 }}
          {{- end }}
      restartPolicy: Never
      {{- with .Values.worker.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.worker.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.worker.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.image.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 6 }}
      {{- end}}
